import 'package:flutter/material.dart';

import 'controller.dart';
import 'model.dart';

class NewsDetailsView extends StatefulWidget {
  final int id;
  NewsDetailsView({this.id});
  @override
  _NewsDetailsViewState createState() => _NewsDetailsViewState();
}

class _NewsDetailsViewState extends State<NewsDetailsView> {
  NewsDetailsController _newsDetailsController = NewsDetailsController();
  NewsDetailsModel _newsDetailsModel = NewsDetailsModel();
  bool _loading = true;

  @override
  void initState() {
    _getDetails();
    super.initState();
  }

  _getDetails() async {
    _newsDetailsModel =
        await _newsDetailsController.getNewsDetials(id: widget.id);
    setState(() {
      _loading = false;
    });

    print(_newsDetailsModel.data.desc);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _loading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 300,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: NetworkImage(_newsDetailsModel.data.image),
                          fit: BoxFit.cover),
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(30),
                        bottomRight: Radius.circular(30),
                      ),
                      color: Colors.red),
                ),
                Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 25),
                    child: Text(_newsDetailsModel.data.title)),
                Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                    child: Text(_newsDetailsModel.data.createAt)),
                Expanded(
                    child: Container(
                  margin: EdgeInsets.all(20),
                  child: Text(_newsDetailsModel.data.desc),
                ))
              ],
            ),
    );
  }
}
