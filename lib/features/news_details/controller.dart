import 'dart:convert';

import 'package:dio/dio.dart';

import 'model.dart';

class NewsDetailsController {
  Dio _dio = Dio();
  NewsDetailsModel _newsDetailsModel = NewsDetailsModel();
  Future<NewsDetailsModel> getNewsDetials({int id}) async {
    Response response =
        await _dio.get('https://association.rowadtqnee.sa/api/lastNews/$id');
    var data = json.decode(response.toString());
    _newsDetailsModel = NewsDetailsModel.fromJson(data);
    return _newsDetailsModel;
  }
}
