import 'package:flutter/material.dart';
import 'package:group1_api/features/categories/controller.dart';
import 'package:group1_api/features/categories/model.dart';

class CategoriesView extends StatefulWidget {
  @override
  _CategoriesViewState createState() => _CategoriesViewState();
}

class _CategoriesViewState extends State<CategoriesView> {
  CategoriesController _categoriesController = CategoriesController();
  CategoriesModel _categoriesModel = CategoriesModel();

  bool _loading = true;

  @override
  initState() {
    _getCats();
    super.initState();
  }

  _getCats() async {
    _categoriesModel = await _categoriesController.getCats();
    setState(() {
      _loading = false;
    });
  }

  Widget _catCard({String image, String name}) {
    List<String> _splitedList = name.split(' ');
    String _shortName = _splitedList[0];

    return Container(
      padding: EdgeInsets.all(7),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: Colors.grey.shade200,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Image.network(
            image,
            width: 50,
            height: 50,
          ),
          Expanded(child: SizedBox()),
          Padding(padding: EdgeInsets.only(right: 5), child: Text(_shortName)),
          SizedBox(
            height: 5,
          ),
          Text(name.replaceRange(0, _shortName.length, '')),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          leading: Icon(
            Icons.arrow_back_ios,
            color: Colors.black,
          ),
          elevation: 0,
          centerTitle: false,
          backgroundColor: Colors.white,
          title: Text(
            'اقسام اللجنه',
            style: TextStyle(color: Colors.black),
          ),
        ),
        body: _loading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Padding(
                padding: EdgeInsets.all(5),
                child: GridView.builder(
                    itemCount: _categoriesModel.data.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        crossAxisSpacing: 5,
                        mainAxisSpacing: 5),
                    itemBuilder: (ctx, index) {
                      return _catCard(
                          image: _categoriesModel.data[index].image,
                          name: _categoriesModel.data[index].name);
                    }),
              ),
      ),
    );
  }
}
