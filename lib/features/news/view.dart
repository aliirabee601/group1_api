import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:group1_api/features/news_details/view.dart';
import 'controller.dart';
import 'model.dart';

class NewsView extends StatefulWidget {
  @override
  _NewsViewState createState() => _NewsViewState();
}

class _NewsViewState extends State<NewsView> {
  NewsController _newsController = NewsController();
  NewsModel _newsModel = NewsModel();
  bool _loading = true;
  @override
  void initState() {
    _getNews();
    super.initState();
  }

  _getNews() async {
    _newsModel = await _newsController.getNews();
    setState(() {
      _loading = false;
    });
  }

  Widget _newsCard({String imageUrl, String title, String date, int id}) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => NewsDetailsView(
                      id: id,
                    )));
      },
      child: Stack(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: 200,
            margin: EdgeInsets.all(10),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage(imageUrl), fit: BoxFit.cover),
                color: Colors.red,
                borderRadius: BorderRadius.circular(10)),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: 200,
            margin: EdgeInsets.all(10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.black.withOpacity(0.4)),
          ),
          Positioned(
              bottom: 20,
              right: 30,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    date,
                    style: TextStyle(color: Colors.white),
                  )
                ],
              ))
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          leading: Icon(
            Icons.arrow_back_ios,
            color: Colors.black,
          ),
          elevation: 0,
          centerTitle: false,
          backgroundColor: Colors.white,
          title: Text(
            'اخبار اللجنه',
            style: TextStyle(color: Colors.black),
          ),
        ),
        body: _loading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : ListView.builder(
                itemCount: _newsModel.data.length,
                itemBuilder: (ctx, index) {
                  return _newsCard(
                      imageUrl: _newsModel.data[index].image,
                      title: _newsModel.data[index].title,
                      id: _newsModel.data[index].id,
                      date: _newsModel.data[index].createAt);
                }),
      ),
    );
  }
}
