import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:group1_api/features/news/model.dart';

class NewsController {
  NewsModel _newsModel = NewsModel();
  Dio _dio = Dio();
  Future<NewsModel> getNews() async {
    var response =
        await _dio.get('https://association.rowadtqnee.sa/api/lastNews');
    var data = json.decode(response.toString());
    _newsModel = NewsModel.fromJson(data);
    return _newsModel;
  }
}
